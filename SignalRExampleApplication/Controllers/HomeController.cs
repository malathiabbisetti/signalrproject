﻿using Newtonsoft.Json;
using RestSharp;
using SignalRExampleApplication.Hubs;
using SignalRExampleApplication.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SignalRExampleApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Get()
        {

            //using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ContactDbContext"].ConnectionString))
            //{
            //    connection.Open();
            //    using (SqlCommand command = new SqlCommand(@"SELECT [ContactID],[ContactName],[ContactNo] FROM [dbo].[Contacts]", connection))
            //    {
            //        // Make sure the command object does not already have
            //        // a notification object associated with it.
            //        command.Notification = null;

            //        SqlDependency dependency = new SqlDependency(command);
            //        dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

            //        if (connection.State == ConnectionState.Closed)
            //            connection.Open();

            //        SqlDataReader reader = command.ExecuteReader();

            //        var listcon = reader.Cast<IDataRecord>()
            //                .Select(x => new
            //                {
            //                    Id = (int)x["ContactID"],
            //                    ContactName = (string)x["ContactName"],
            //                    ContactNo = (string)x["ContactNo"],
            //                }).ToList();

            //        return Json(new { listcon = listcon }, JsonRequestBehavior.AllowGet);

            //    }
            //}
            List<Contact> listcon = new List<Contact>();
            var data = string.Format("{0}/api/Values/GetContacts", "http://localhost:59156/");
            var result = this.GetResponse(data);
            if (result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                dynamic contactListData = JsonConvert.DeserializeObject(result.Content);
                listcon = contactListData.contactsList.ToObject<List<Contact>>();
            }
            return Json(new { listcon = listcon }, JsonRequestBehavior.AllowGet);
        }

        public static void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            ContactHub.Show();
        }

        public IRestResponse GetResponse(string url)
        {
            RestClient client = null;
            client = new RestClient(url);
            RestRequest request = null;
            request = new RestRequest(Method.GET);
            ////request.AddHeader("cache-control", "no-cache");
            ////request.AddHeader("content-type", "application/json");
            request.Timeout = 300 * 1000;
            var response = client.Execute(request);
            return response;
        }
    }
}