﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignalRExampleApplication.Models
{
    public class Contact
    {
        public int ContactId { get; set; }

        public string ContactName { get; set; }

        public string ContactNo { get; set; }

        public DateTime AddedOn { get; set; }

        public bool Status { get; set; }
    }
}