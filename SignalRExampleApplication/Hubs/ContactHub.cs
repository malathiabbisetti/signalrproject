﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignalRExampleApplication.Hubs
{
    public class ContactHub : Hub
    {
        public static void Show()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<ContactHub>();
            context.Clients.All.displayContact();
        }
    }
}