﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using SignalRExampleApplication.Hubs;

[assembly: OwinStartup(typeof(SignalRExampleApplication.Startup))]

namespace SignalRExampleApplication
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ////ConfigureAuth(app);
            ////app.MapSignalR();
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888

            app.Map("/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new HubConfiguration
                {
                    EnableJSONP = true
                };
                map.RunSignalR(hubConfiguration);
            });

            
        }

    }
}
