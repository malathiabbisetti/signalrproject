﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using SignalRExampleApplication.Hubs;

[assembly: OwinStartup(typeof(WebAPI.Startup))]

namespace WebAPI
{
    public class HubContact : Hub
    {
        public static void Show()
        {
            //IHubContext context = GlobalHost.ConnectionManager.GetHubContext<HubContact>();
            //context.Clients.All.displayContact();
            var hubConnection = new HubConnection("http://localhost:55896/");
            IHubProxy chatHubProxy = hubConnection.CreateHubProxy("contactHub");
            hubConnection.Start().Wait();
            chatHubProxy.On("Show",()=> {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<ContactHub>();
                hubContext.Clients.All.displayContact();
            });
        }
    }

    public class Startup
    {
        static HubConnection hubConnection;

        public  void Configuration(IAppBuilder app)
        {
            ////app.MapSignalR();
            app.Map("/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new Microsoft.AspNet.SignalR.HubConfiguration()
                {
                    EnableJSONP = true,
                    EnableDetailedErrors = true
                };
                map.RunSignalR(hubConfiguration);
            });
            ////await SetHub();
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
        }

        private async Task SetHub()
        {
            hubConnection = new HubConnection("http://localhost:55896/signalr", useDefaultUrl: false);
            IHubProxy chatHubProxy = hubConnection.CreateHubProxy("hubContact");
            chatHubProxy.On("Show", () =>
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<HubContact>();
                hubContext.Clients.All.displayContact();

            });

            chatHubProxy.On("displayContact", () =>
            {
                chatHubProxy.Invoke("displayContact");
            });

            await hubConnection.Start();
        }
    }
}
