﻿using Newtonsoft.Json;
using SignalRExampleApplication.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using SignalRExampleApplication.Controllers;
using SignalRExampleApplication.Hubs;

namespace WebAPI.Controllers
{
    public class ValuesController : ApiController
    {
        //// GET api/values
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/values/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/values
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/values/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/values/5
        //public void Delete(int id)
        //{
        //}

        public HttpResponseMessage GetContacts()
        {
            var response = new HttpResponseMessage();
            //ContactRepository contactRepository = new ContactRepository();

            //var data = contactRepository.Get();
            //Dictionary<string, object> offeringsList = new Dictionary<string, object>();
            //offeringsList.Add("contactsList", data);
            //var result = Newtonsoft.Json.JsonConvert.SerializeObject(offeringsList);
            //response.StatusCode = HttpStatusCode.OK;
            //response.Content = new StringContent(result, System.Text.Encoding.UTF8, "text/plain");
            //return response;

            var connection = new SqlConnection("Data Source=192.168.0.50;Initial CataLog=Training_SignalR;User Id=tmg;Password=TMG");

            connection.Open();
            using (SqlCommand command = new SqlCommand(@"SELECT [ContactID],[ContactName],[ContactNo] FROM [dbo].[Contacts]", connection))
            {
                // Make sure the command object does not already have
                // a notification object associated with it.
                command.Notification = null;

                SqlDependency dependency = new SqlDependency(command);
                dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                var listcon = reader.Cast<IDataRecord>()
                        .Select(x => new Contact
                        {
                            ContactId = (int)x["ContactID"],
                            ContactName = (string)x["ContactName"],
                            ContactNo = (string)x["ContactNo"],
                        }).ToList();
                //List<Contact> contactlist = new List<Contact>();
                //contactlist.AddRange(listcon);
                Dictionary<string, object> offeringsList = new Dictionary<string, object>();
                offeringsList.Add("contactsList", listcon);
                var result = JsonConvert.SerializeObject(offeringsList);
                response.StatusCode = HttpStatusCode.OK;
                response.Content = new StringContent(result, Encoding.UTF8, "text/plain");

                return response;
                ////return listcon.ToList();

            }
        }


        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            ////Hubs.HubContact.Show();
            HubContact.Show();
        }
    }
}
