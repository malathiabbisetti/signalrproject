﻿using Newtonsoft.Json;
using SignalRExampleApplication.WebApi.Hubs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;

namespace SignalRExampleApplication.WebApi.Controllers
{
    public class ValuesController : ApiController
    {
        //// GET api/values
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/values/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        [HttpGet]
        public HttpResponseMessage Get()
        {
            var response = new HttpResponseMessage();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ContactDbContext"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(@"SELECT [ContactID],[ContactName],[ContactNo] FROM [dbo].[Contacts]", connection))
                {
                    // Make sure the command object does not already have
                    // a notification object associated with it.
                    command.Notification = null;

                    SqlDependency dependency = new SqlDependency(command);
                    dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    var listcon = reader.Cast<IDataRecord>()
                            .Select(x => new
                            {
                                Id = (int)x["ContactID"],
                                ContactName = (string)x["ContactName"],
                                ContactNo = (string)x["ContactNo"],
                            }).ToList();

                    Dictionary<string, object> offeringsList = new Dictionary<string, object>();
                    offeringsList.Add("contactsList", listcon);
                    var result = JsonConvert.SerializeObject(offeringsList);
                    response.StatusCode = HttpStatusCode.OK;
                    response.Content = new StringContent(result, Encoding.UTF8, "text/plain");

                    return response;
                }
            }
        }

        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            ContactHub.Show();
        }
    }
}
