﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

////[assembly: OwinStartup(typeof(SignalRExampleApplication.WebApi.Startup))]

namespace SignalRExampleApplication.WebApi
{

    public class ContactHub : Hub
    {
        public static void Show()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<ContactHub>();
            context.Clients.All.displayContact();
            ////Clients.All.displayContact();
        }
    }
    public class Startup
    {
        //public void Configuration(IAppBuilder app)
        //{
        //    ////app.MapSignalR();
        //    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
        //}

        static HubConnection hubConnection;

        public async void Configuration(IAppBuilder app)
        {
            app.Map("/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new HubConfiguration
                {
                    EnableJSONP = true
                };
                map.RunSignalR(hubConfiguration);
            });
            ////await Sethub();
        }

        private async Task Sethub()
        {
            hubConnection = new HubConnection("http://localhost:55896/signalr", useDefaultUrl: false);
            IHubProxy chatHubProxy = hubConnection.CreateHubProxy("ContactHub");

            chatHubProxy.On("Show", () =>
             {
                 var hubContext = GlobalHost.ConnectionManager.GetHubContext<ContactHub>();
                 hubContext.Clients.All.displayContact();

             });

            chatHubProxy.On("displayContact", () =>
             {
                 chatHubProxy.Invoke("displayContact");
             });

            await hubConnection.Start();
        }
    }
}
