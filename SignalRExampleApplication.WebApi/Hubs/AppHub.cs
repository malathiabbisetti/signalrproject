﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace SignalRExampleApplication.WebApi.Hubs
{
    public class AppHub : Hub
    {
        public static void Show()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<AppHub>();
            context.Clients.All.displayContact();
        }
    }
}